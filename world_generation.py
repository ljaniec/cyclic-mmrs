# coding/utf-8
from namedlist import namedlist
import math
import numpy as np

# for vector of robots' coordinates at start
Robot = namedlist('Robot', ['id', 'X', 'Y', 'Theta'])

def worldGeneration(name, n, workspaceX, workspaceY, cell):
    '''Generator of world file name_nZ.world for Stage with n robots in workspace of [workspaceX x workspaceY] meters (private cells involved outside). 
    
    Creates file name_nZ.world, Z - number of robots. 
    (Appends lines, not overwrite, for not truncating existing files.)
    Returns vector of starting triplets for robots, i.e. [(X0, Y0, Theta0),...]
    
    Starting angles are oriented toward workspace, each
    robot has own private cell at start (square bigger than robot!)

    Arguments:
        name {[str]} -- [name of *.world file]
        n {uint} -- [number of robots]
        workspaceX {[float]} -- [size of workspace, m, OX]
        workspaceY {[float]} -- [size of workspace, m, OY]
        cell  {[float]} -- [size of starting cell (square), m]    
    '''
    # preparing vector of cells around workspace for robots
    # Left -> Right -> Top -> Bottom
    sizeX = workspaceX + 2.0 * cell
    sizeY = workspaceY + 2.0 * cell
    widthCells = int(workspaceX/cell)
    heightCells = int(workspaceY/cell)
    # assumption - there are always robots <= cells
    freeCells = [0] * (2 * (widthCells + heightCells))
    if len(freeCells) < n:
        raise ValueError("More robots than cells around workspace!")
    # vector of namedlist Robot(id, X0, Y0, Theta0)
    robots = [Robot(0, -1, -1, 0) for r in range(n)]       
    # standard part of every *.world file for us
    stnd_part =[
        "define floorplan model",
        "(",
        '  color "gray30"',
        '  boundary 1',
        '  gui_nose 1 # 1 if world orientation is important (def: to right)',
        '  gui_grid 1 # if with grid',
        '  gui_outline 0',
        '  gripper_return 0',
        '  fiducial_return 0',
        ')',

        '# define newname oldname - position model simulates a mobile robot base',
        'define turtlebot position',
        '(',
        '      size [0.7 0.7 0.8]',
        '      # shape of turtlebot',
        '      block',
        '      (',
        '            points 8',
        '            point[0] [0.75 0]',
        '            point[1] [1 0.25]',
        '            point[2] [1 0.75]',
        '            point[3] [0.75 1]',
        '            point[4] [0.25 1]',
        '            point[5] [0 0.75]',
        '            point[6] [0 0.25]',
        '            point[7] [0.25 0]',
        '            z [0 0.5]',
        '      )',
        '      # position properties',
        '      drive "diff"',
        '',
        '      velocity [ 0.0 0.0 0.0 0.0 ]',
        '',
        '      localization "gps"',
        '      localization_origin [0 0 0 0]',
        '',
        '      # [ xmin xmax ymin ymax zmin zmax amin amax ]',
        '      velocity_bounds [-0.65 0.65 -0.65 0.65 -0.65 0.65 -110 110]',
        '',      
        '      acceleration_bounds [-0.6 0.6 -0.6 0.6 0 0 -90 90]',
        ')',
        '',
        '# set the resolution of the underlying raytrace model in meters',
        'resolution 0.01',
        '# simulation timestep in milliseconds',
        'interval_sim 10',
        "# for clock of simulation's time",
        'show_clock                1',
        'show_clock_interval     100',
        'window',
        '( ',
        '  size [900  900]',
        '  rotate [0.000 0.000]',
        '  scale ' + str(int(0.8*900/max(workspaceX, workspaceY))) + 
            ' # should be window_size/floorplan_size rounded downwards',
        ')',
        'floorplan',
        '(',
        '  name "workspace"',
        '  bitmap "workspace.png"',
        '  size [ '+str(sizeX)+' '+str(sizeY)+' -1.0]',
        '  pose [ '+str(sizeX/2.0)+' '+str(sizeY/2.0)+' 0.000 0.000 ]',
        ')'
]
    # choice of cell for every robots around workspace
    with open(str(name)+"_n"+str(n)+'.world', "w") as file:
        for p in stnd_part:
            file.write(p + '\n')
        for robotID in range(n):
            body = [ # same for every robot
                'turtlebot',
                '(',
                '  name "robot_'+str(robotID)+'"',
                '  color "red"'
            ]
            for b in body:
                file.write(b + '\n')
            robots[robotID].id = robotID
            idCell = next( (it for it, cell in enumerate(freeCells) if not cell), None)
            freeCells[idCell] = 1
            if robotID < heightCells: # Left
                robotX = cell/2.0 
                robotY = 1.5 * cell + idCell * cell
                robotTheta = 0
                #coordinates
                for b in ['pose [', str(robotX), ' ', str(robotY), ' 0.0 ', str(robotTheta), ']\n']:
                    file.write(b)
                file.write(')\n')
                robots[robotID].X = robotX
                robots[robotID].Y = robotY
                robots[robotID].Theta = robotTheta
                continue
            elif robotID < (2 * heightCells): # Right
                robotX = -cell/2.0 + sizeX
                robotY = -1.5 * cell - (idCell+2) * cell + 2.0*sizeY
                robotTheta = 180
                #coordinates
                for b in ['pose [', str(robotX), ' ', str(robotY), ' 0.0 ', str(robotTheta), ']\n']:
                    file.write(b)
                file.write(')\n')
                robots[robotID].X = robotX
                robots[robotID].Y = robotY
                robots[robotID].Theta = robotTheta
                continue
            elif robotID < (2 * heightCells + widthCells): # Top
                robotX = cell/2.0 - idCell * cell + 2.5 * sizeX
                robotY = -cell/2.0 + sizeY
                robotTheta = 270
                #coordinates
                for b in ['pose [', str(robotX), ' ', str(robotY), ' 0.0 ', str(robotTheta), ']\n']:
                    file.write(b)
                file.write(')\n')
                robots[robotID].X = robotX
                robots[robotID].Y = robotY
                robots[robotID].Theta = robotTheta
                continue
            elif robotID < (2 * heightCells + 2 * widthCells): # Bottom
                robotX = -1.5 * cell -idCell * cell + 3.5 * sizeX
                robotY =  cell/2.0 
                robotTheta = 90
                #coordinates
                for b in ['pose [', str(robotX), ' ', str(robotY), ' 0.0 ', str(robotTheta), ']\n']:
                    file.write(b)
                file.write(')\n')
                robots[robotID].X = robotX
                robots[robotID].Y = robotY
                robots[robotID].Theta = robotTheta
                continue
            else:
                raise ValueError("More robots than cells.")
    return robots


def pathsGeneration(robots, wspaceX, wspaceY, nPts, cell):
    '''Generator of paths for unicycle, consisting of line segments only. 
    Path starts close to private cell of robot, first and last point from nPts overall are 
    on line segment, which is perpendicular to the bounds of workspace.
    
    Returns vector paths, consisting of path (like described above) for every robot with ID, i.e.
    paths[id] = [(X0, Y1), (X1, Y1),...,(XnPts, YnPts)]

    Arguments:
        robots {[namedlist]} -- vector of namedlist('Robot', ['id', 'X', 'Y', 'Theta'])
        wspaceX {[float]} -- [size of workspace, m, OX]
        wspaceY {[float]} -- [size of workspace, m, OY]
        nPts {[int]} -- [number of points in path for robots]
        cell {[float]} -- [size of starting cell]
    '''
    paths = [ [(robot.X, robot.Y)] for robot in robots] # first in cycle (starting point)
    for robot in robots:
        id = robot.id
        X0 = robot.X
        Y0 = robot.Y
        Theta0 = robot.Theta
        paths[id].append((X0 + math.cos(Theta0) * cell, Y0 + math.sin(Theta0) * cell)) # second in cycle, close to start
        if nPts - 4 < 0:
            raise ValueError('Error! nPts - 4 is negative, need more points in path.')
        Xs = np.random.uniform(low= cell, high=wspaceX - cell, size=(nPts - 4)).astype(float)
        Ys = np.random.uniform(low= cell, high=wspaceY - cell, size=(nPts - 4)).astype(float)
        # minus 4, because in one cycle we already know first and second point, altogether with last and one before
        for p in range(nPts - 4):
            paths[id].append((Xs[p], Ys[p]))
        paths[id].append((X0 + math.cos(Theta0) * cell, Y0 + math.sin(Theta0) * cell)) # one before last in cycle
        paths[id].append((X0, Y0)) # last in cycle
    # for id, path in enumerate(paths[:2]):
        # print 'Robot '+str(id)+' has path in cycle:' 
        # print path
    return paths

def addCycles(paths, nRepeats):
    ''' Adds nRepeats of cycle for every path in paths
    
    Arguments:
        paths {[vector of lists of tuples]} -- paths of robots
        nRepeats {[uint]} -- number of repeats of cycle for every robot
    '''
    for path in paths[:2]:
        for n in range(nRepeats):
            path.extend(path[1:])
        # print "Robot:\n", path
    return paths

if __name__ == '__main__':
    name = 'tmp'
    n = 40 # number of robots
    workspaceX = 10 # m
    workspaceY = 10 # m
    cell = 1 # m, size of private cell
    robots = worldGeneration(name, n, workspaceX, workspaceY, cell)
    nPts = 6
    paths = pathsGeneration(robots, workspaceX, workspaceY, nPts, cell)
    nRepeats = 3
    paths = addCycles(paths, nRepeats)
    print paths[:2]