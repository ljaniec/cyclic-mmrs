#!/usr/bin/env python

import copy
import math
from resource import Resource

# tu sa rzeczy dotyczace podzialu przestrzeni na sektory, nie chce mi sie tego
# komentowac bo to chyba malo istotne jest

# x_crossed/y_crossed - True if actual state acquires more than 1 resource
# in horizontal/vertical axis
def nextResources(previous, x_crossed, y_crossed, x_rising, y_rising):
    res = copy.deepcopy(previous)
    if len(previous.tiles) == 1:
        central_tile = res.tiles[0]
        if x_crossed:
            if x_rising:
                res.tiles.append((central_tile[0]+1, central_tile[1]))
            else:
                res.tiles.append((central_tile[0]-1, central_tile[1]))
        if y_crossed:
            if y_rising:
                res.tiles.append((central_tile[0], central_tile[1]+1))
            else:
                res.tiles.append((central_tile[0], central_tile[1]-1))
        if x_crossed and y_crossed:
            res.tiles.append((res.tiles[1][0], res.tiles[2][1]))  
    elif len(previous.tiles) == 2:
        if previous.tiles[0][0] == previous.tiles[1][0]: # horizontal rectangle
            if x_crossed and y_crossed:
                if x_rising:
                    res.tiles.append((res.tiles[0][0]+1, res.tiles[0][1]))
                    res.tiles.append((res.tiles[1][0]+1, res.tiles[1][1]))
                else:
                    res.tiles.append((res.tiles[0][0]-1, res.tiles[0][1]))
                    res.tiles.append((res.tiles[1][0]-1, res.tiles[1][1]))
            elif (not y_crossed) and (not x_crossed):
                if res.tiles[0][1] > res.tiles[1][1]:
                    if y_rising:
                        del res.tiles[1]
                    else:
                        del res.tiles[0]
                else:
                    if y_rising:
                        del res.tiles[0]
                    else:
                        del res.tiles[1]
            else: # we are in vertical rectangle now
                if res.tiles[0][1] > res.tiles[1][1]:
                    if y_rising:
                        del res.tiles[1]
                    else:
                        del res.tiles[0]
                else:
                    if y_rising:
                        del res.tiles[0]
                    else:
                        del res.tiles[1]
                if x_rising:
                    res.tiles.append((res.tiles[0][0]+1, res.tiles[0][1]))
                else:
                    res.tiles.append((res.tiles[0][0]-1, res.tiles[0][1]))
        else: # vertical rectangle
            if x_crossed and y_crossed:
                if y_rising:
                    res.tiles.append((res.tiles[0][0], res.tiles[0][1]+1))
                    res.tiles.append((res.tiles[1][0], res.tiles[1][1]+1))
                else:
                    res.tiles.append((res.tiles[0][0], res.tiles[0][1]-1))
                    res.tiles.append((res.tiles[1][0], res.tiles[1][1]-1))
            elif (not y_crossed) and (not x_crossed):
                if res.tiles[0][0] > res.tiles[1][0]:
                    if x_rising:
                        del res.tiles[1]
                    else:
                        del res.tiles[0]
                else:
                    if x_rising:
                        del res.tiles[0]
                    else:
                        del res.tiles[1]
            else: # we are in horizontal rectangle now
                if res.tiles[0][0] > res.tiles[1][0]:
                    if x_rising:
                        del res.tiles[1]
                    else:
                        del res.tiles[0]
                else:
                    if x_rising:
                        del res.tiles[0]
                    else:
                        del res.tiles[1]
                if y_rising:
                    res.tiles.append((res.tiles[0][0], res.tiles[0][1]+1))
                else:
                    res.tiles.append((res.tiles[0][0], res.tiles[0][1]-1))
    else:  # len == 4
        if x_crossed and not y_crossed:
            res.deleteRow(y_rising)
        elif y_crossed and not x_crossed:
            res.deleteColumn(x_rising)
        else:  # we acquire only one resource
            #print len(res.tiles)
            if y_rising:
                y = max(res.tiles[0][1], res.tiles[1][1], res.tiles[2][1], res.tiles[3][1])
            else:
                y = min(res.tiles[0][1], res.tiles[1][1], res.tiles[2][1], res.tiles[3][1])
            if x_rising:
                x = max(res.tiles[0][0], res.tiles[1][0], res.tiles[2][0], res.tiles[3][0])
            else:
                x = min(res.tiles[0][0], res.tiles[1][0], res.tiles[2][0], res.tiles[3][0])
            res = Resource()
            res.tiles.append((x,y))
    return res
                

def resourcesInPoint(point, r, d):
    # assuming point is valid (inside boundaries)
    x_mod = point[0] % d
    y_mod = point[1] % d
    central = (int((point[0]-x_mod)/d), int((point[1]-y_mod)/d))
    res = Resource()
    res.tiles.append(central)
    if x_mod < r:
        res.tiles.append((central[0]-1, central[1]))
    elif x_mod > d-r:
        res.tiles.append((central[0]+1, central[1]))
    if y_mod < r:
        res.tiles.append((central[0], central[1]-1))
    elif y_mod > d-r:
        res.tiles.append((central[0], central[1]+1))
    # my modification - allocating 4th resource between res.tiles[1] and [2]
    if len(res.tiles) == 3:
        res.tiles.append((res.tiles[1][0], res.tiles[2][1]))
    # in exact implementation we should rather measure distances from point to
    # corners, and add 4th resource only if one of them is less then r
    return res

            
def segment2resourceList(begin, end, r, d):
    # x axis
    if begin[0] > end[0]:
        x_min = end[0]
        x_max = begin[0]
    else:
        x_max = end[0]
        x_min = begin[0]
    
    x_tess = math.floor(x_min/d) * d   #!!!!!!!!
    x_tess2 = x_tess
    x_dividers = []
    while True:
        if (x_tess - r) > x_min and (x_tess - r) < x_max:
            x_dividers.append(x_tess - r)
        elif (x_tess - r) > x_max:
            break
        if (x_tess + r) > x_min and (x_tess + r) < x_max:
            x_dividers.append(x_tess + r)
        elif (x_tess + r) > x_max:
            break
        x_tess += d
    #print x_min, x_max, x_tess2
    if begin[0] < end[0]:
        x_rising = True
        if (x_min - x_tess2) < r or (x_min - x_tess2) > (d - r):
            x_started_in_the_middle = False
        else:
            x_started_in_the_middle = True
    else:
        x_rising = False
        x_dividers.reverse()
        x_tess2 = math.floor(x_max/d) * d
        if (x_max - x_tess2) < r or (x_max - x_tess2) > (d - r):
            x_started_in_the_middle = False
        else:
            x_started_in_the_middle = True


    # y axis
    if begin[1] > end[1]:
        y_min = end[1]
        y_max = begin[1]
    else:
        y_max = end[1]
        y_min = begin[1]
    y_tess = math.floor(y_min/d) * d
    y_tess2 = y_tess
    #print "ymin minus ytess", y_min 
    
    y_dividers = []
    while True:
        if (y_tess - r) > y_min and (y_tess - r) < y_max:
            y_dividers.append(y_tess - r)
        elif (y_tess - r) > y_max:
            break
        if (y_tess + r) > y_min and (y_tess + r) < y_max:
            y_dividers.append(y_tess + r)
        elif (y_tess + r) > y_max:
            break
        y_tess += d
    if begin[1] < end[1]:
        y_rising = True
        if (y_min - y_tess2) < r or (y_min - y_tess2) > (d - r):
            y_started_in_the_middle = False
        else:
            y_started_in_the_middle = True
    else:
        y_rising = False
        y_dividers.reverse()
        y_tess2 = math.floor(y_max/d) * d
        if (y_max - y_tess2) < r or (y_max - y_tess2) > (d - r):
            y_started_in_the_middle = False
        else:
            y_started_in_the_middle = True


    #print x_dividers
    #print y_dividers
    #print x_started_in_the_middle, y_started_in_the_middle
    
    # line equation
    if (begin[0]-end[0]) != 0:
        a = (begin[1]-end[1])/(begin[0]-end[0])
        b = (begin[1] - ((begin[1]-end[1])/(begin[0]-end[0]))*begin[0])
    
    # main algorithm 
    res1 = [resourcesInPoint(begin, r, d)]
    #print "Zaczynam od resource:", res1[0].tiles
    while len(x_dividers) != 0 or len(y_dividers) != 0:
        if len(y_dividers) == 0:
            take_x = True
        elif len(x_dividers) == 0:
            take_x = False
        else:
            y1 = y_dividers[0]
            y2 = a*x_dividers[0] + b
            if y_rising:
                if y1 < y2:
                    take_x = False
                else:
                    take_x = True
            else:
                if y1 < y2:
                    take_x = True
                else:
                    take_x = False
        if take_x:
            del x_dividers[0]
            x_started_in_the_middle = not x_started_in_the_middle
        else:
            del y_dividers[0]
            y_started_in_the_middle = not y_started_in_the_middle
       
        prev = copy.deepcopy(res1[-1])
        next_resource = nextResources(prev, not x_started_in_the_middle, not y_started_in_the_middle, x_rising, y_rising)
        #print "x_started_in_the_middle:", x_started_in_the_middle
        #print "y_started_in_the_middle:", y_started_in_the_middle
        #print "x_rising:", x_rising
        ##print "y_rising:", y_rising
        #print "dostalem wiec:",  next_resource.tiles
        res1.append(next_resource)  
    return res1
        
# returns empty list if passed path is not feasible (e.g. outside map borders)
def path2resourceList(path,r,d):
    resource_list = []
    for p in path:
        if len(p) == 0:
            continue
        res = [resourcesInPoint(p[0], r, d)]
        for i in range(len(p)-1):
            segment_res = segment2resourceList(p[i], p[i+1], r, d)
            if len(segment_res) != 0:
                del segment_res[0]
            res = res + segment_res
        resource_list.append(res)
        
    # bruteforce method for finding private sectors
    for i in range(len(resource_list)):
        for sec1 in resource_list[i]:
            colliding = False
            for j in range(len(resource_list)):
                if i == j:
                    continue
                for sec2 in resource_list[j]:
                    if sec1.isColliding(sec2):
                        colliding = True
                        break
                if colliding:
                    break
            if not colliding:
                sec1.isPrivate = True
                       
    return resource_list

'''begin = (2.78, 0.26)
end = (1.35, 2.55)
r = 0.15
d = 0.8
res = segment2resourceList(begin, end, r, d)
for x in res:
    print x.tiles'''

