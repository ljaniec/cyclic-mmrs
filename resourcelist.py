#!/usr/bin/env python

import copy
import time

# tu jest klasa przechowujaca liste sektorow wchodzacych w sklad sciezek
# kazdego robota. Ogolnie to potrafi ona na podstawie stanu (wektora aktualnych
# sektorow wszystkich robotow) ocenic czy ten stan jest dobry czy nie. Ma
# zastosowanie tylko do algorytmow opartych na teselacji, wiec malo istotne

class ResourceList:
    def __init__(self, _res):
        self.res = _res
        # lista czasow osiagniecia klejnych etapow dla kazdej sciezki
        self.times = [None] * len(self.res)

    def startCountingTime(self):
        for i in range(len(self.times)):
            self.times[i] = [time.time()]

    def display(self):
        for path_id in range(len(self.res)):
            # for r in self.res[path_id]:
                # print r.tiles, " is private: "
                # print r.isPrivate
                # print '-------'
            r = self.res[path_id][0]
            print r.tiles, " is private: "
            print r.isPrivate
            print '-------'    
            print "#### end of path ", path_id, " ###"
            
    def displayTimes(self):
        for path_id in range(len(self.times)):
            print "Stage  0 :  0s"
            for i in range(1,len(self.times[path_id])):
                print "Stage ", i, ": ", self.times[path_id][i] - self.times[path_id][i-1], "s"
            print "#### end of path ", path_id, " ###"
    

    def isFeasible(self, state):
        if self.isColliding(state):
            return False
        return self.isPOrdered(state)

    def isColliding(self, state):
        #print "DEBUG:", state
        for i in range(len(state)-1):
            for j in range(i+1, len(state)):
                if self.res[i][state[i]].isColliding(self.res[j][state[j]]):
                    return True
        return False

    # improved version od isOrdered() function
    def isPOrdered(self, state):
        occupied = []

        for i in range(len(state)):
            occupied += self.res[i][state[i]].tiles

        #print "occupied wyszlo takie: ", occupied

        # list of all resources ever nedded to complete a task
        # it's list of RESOURCES, not SECTORS
        remain = []
        for i in range(len(state)):
            remain_i = []
            for j in range(state[i],len(self.res[i])):
                if self.res[i][j].isPrivate:
                    break
                remain_i += self.res[i][j].tiles
            remain.append(list(set(remain_i)))

        #print "remain wyszlo takie: ", remain

        vehicle_list = range(len(state))
        while True:
            lack_of_progress = True
            new_list = copy.deepcopy(vehicle_list)
            for i in vehicle_list:
                # resources needed for advancement, but currently occupied
                intersection = [val for val in remain[i] if val in occupied]
                #print "intersection wyszlo takie: ", intersection
                #print "a ma byc takie, zeby mozna bylo usunac to zadanie: ", self.res[i][state[i]].tiles
                if set(intersection) == set(self.res[i][state[i]].tiles) or remain[i] == []:
                    new_list.remove(i)
                    for x in self.res[i][state[i]].tiles:
                        occupied.remove(x)
                    lack_of_progress = False
            vehicle_list = copy.deepcopy(new_list)
            if len(new_list) == 0 or lack_of_progress:
                break
        if len(vehicle_list) == 0:
            print "stan ", state, " jest bezpieczny!"
            return True
        
        print "stan ", state, " jest nieuporzadkowany!"
        return False
            
