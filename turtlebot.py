#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from nav_msgs.msg import Odometry
from std_msgs.msg import UInt8
from math import pow, atan2, sqrt
import numpy as np
import tesselation as tess

# jakies stale symboliczbe, malo istotne
MAX_LIN_VEL = 5
MAX_ANG_VEL = 4
EPSILON_VEL = 0.1
EPSILON_ANG = 0.1

# to musi byc ustawione jak zmieniasz zadanie, powinno sie to robic
# automatycznie, ale coz xD
# # only applies to empty.world world file
# OFFSET_X = [2.72, 4.33, 1.88, 4.16, 5.43, 3.39 ]
# OFFSET_Y = [0.23, 0.3, 4.32, 4.3, 1.79, 4.29 ]
#OFFSET_THETA = [0, -38.532]


class TurtleBot:
    def __init__(self,id,r,d,path):
        # assuming there is more than 1 vehicle, otherwise topics names will
        # be different
        self.velocity_publisher = rospy.Publisher('/robot_'+str(id)+'/cmd_vel',
                                                  Twist, queue_size=10)
        self.request_publisher = rospy.Publisher('/crossing_requests', UInt8,queue_size=10)
        self.pose_subscriber = rospy.Subscriber('/robot_'+str(id)+'/odom', Odometry, self.update_pose)
        # malo wazne parametry
        self.rate = rospy.Rate(10)
        self.id = id
        self.r = r
        self.d = d
        # lista sektorow, ktore robot ma kolejno odwiedzac
        self.path = path
        # aktualna pozycja, aktualizowana w callbacku podlaczonym pod temat
        # z odometria
        self.pose = Pose()
        # flagi powodujace zatrzymanie z roznych powodow
        self.crossing_stop = False
        self.goal_reached = True
        self.stage_counter = 0 # counts goals reached, used by outside code
        self.state_counter = 0 # counts current state

    # no ta funkcja wykrywa czy sektor w ktorym przebywamy to jest ten, w
    # ktorym powinnismy byc, czy juz kolejny. Jak kolejny to szybko sie
    # zatrzymujemy i pytamy sie czy mozemy do niego wjechac xD. Troche za pozno
    # ale w ten sposob mozna rozwiazac to latwo, bo trudniej jest okreslic
    # punkt w ktorym zaraz wjade do sektora, ale jeszcze nie wjechalem. 
    def check_crossing(self,point):
        if self.crossing_stop:
            return
        current_resources = tess.resourcesInPoint(point, self.r, self.d)
        # order of resources on lists is not specified, so casting on sets
        # is crucial here
        if set(current_resources.tiles) == set(self.path[self.state_counter].tiles):
            return
        self.crossing_stop = True
        msg = UInt8()
        msg.data = self.id
        self.request_publisher.publish(msg)
        
        
    # to jest wspomniany callback od odometrii. Aktualizuje nam pose caly czas.
    # dodatkowo sprawdza czy nasze pose to nie jest juz punkt, ktory mamy
    # osiagnac, jak tak to sie zatrzymuje, jak nie to wywoluje move_step(),
    # ktore dostosowuje na biezaco nasza predkosc w zaleznosci od tego jaka
    # jest nasza pozycja wzgledem punktu goal
    def update_pose(self, data):
        self.pose.x = data.pose.pose.position.x# + OFFSET_X[self.id]
        self.pose.y = data.pose.pose.position.y# + OFFSET_Y[self.id]
        self.pose.theta = 2*atan2(data.pose.pose.orientation.z, data.pose.pose.orientation.w)
        self.pose.x = round(self.pose.x, 4)  
        self.pose.y = round(self.pose.y, 4)
        self.check_crossing((self.pose.x,self.pose.y))
        if not self.goal_reached:
            self.move_step()
            


    # funkcje pomocnicze dla sterownika proporcjonalnego, byly w oryginalnym
    # przykladzie z neta. Mozna samemu rozkminic co robia
    def euclidean_distance(self, goal_pose):
        """Euclidean distance between current pose and the goal."""
        return sqrt(pow((goal_pose.x - self.pose.x), 2) +
                    pow((goal_pose.y - self.pose.y), 2))

    def linear_vel(self, goal_pose, constant=1.5):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        return constant * self.euclidean_distance(goal_pose)

    def steering_angle(self, goal_pose):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        return atan2((goal_pose.y - self.pose.y), (goal_pose.x - self.pose.x))

    def angular_vel(self, goal_pose, constant=6):
        """See video: https://www.youtube.com/watch?v=Qh15Nol5htM."""
        angle = (self.steering_angle(goal_pose) - self.pose.theta)
        # always search for the smaller angle
        if angle > np.pi:
            angle -= 2*np.pi
        if angle < -np.pi:
            angle += 2*np.pi
        return constant * angle

    # bardzo wazna funkcja wywolywana przez kod zewnetrzny, opisana szerzej w
    # control.py. Przypomne tu tylko jeszcze raz co robi - zadaje sie przez nia
    # punkt do ktorego turtlebot ma pojechac, i on natychmiast sie obraca w
    # jego kierunku i jedzie tam po prostej
    def move2goal(self, goal):
        self.goal_pose = Pose()
        self.goal_pose.x = goal[0]
        self.goal_pose.y = goal[1] # zamiana tuple na Pose()
        # jak tylko ustawimy sobie punkt docelowy, to ustawiamy tez flage, ze
        # punkt docelowy nie zostal osiagniety, i konczymy funkcje. Zeby wiele
        # robotow moglo jechac rownoczesnie, to caly ruch musi sie odbywac w
        # przerwaniach!! Konkretnie w funkcji move_step(), wywolywanej za
        # kazdym razem jak dostane nowe dane z odometrii, a wiec dosc czesto
        self.goal_reached = False

    # pozwolenie robotowi na przekroczenie granicy. Wywolywane przez kod
    # zewnetrzny
    def allow(self):
        self.crossing_stop = False
        self.state_counter += 1
            
        

    # funkcja odpowiedzialna za ruch. Patrzy sie gdzie jestesmy, i gdzie jest
    # punkt docelowy. Jak nie jestesmy dobrze obroceni w jego strone, to
    # wykonuje sie 1 czesc, czyli obracanie w jego strone. Jak jestesmy, to
    # wykonuje sie 2 czesc, czyli jechanie w jego strone po prostej. Jedno i
    # drugie dziala na zasadzie sterownika P, patrzymy jaki jest blad
    # korygowanej wartosci, mnozymy go przez jakis wspolczynnik, i ustawiamy
    # to co wydjzie jako predkosc robota, katowa (1 czesc) lub liniowa
    # (2 czesc). Zreszta to akurat jest opisane bardzo dobrze w pdf z
    # magisterka, nie wiem po co pisze to tutaj tez xD
    def move_step(self):
        vel_msg = Twist()

        # jesli crossing stop (czekamy an pozwolenie przejazdu) to daj
        # zerowa predkosc
        if self.crossing_stop:
            vel_msg.linear.x = 0
            vel_msg.angular.z = 0
            self.velocity_publisher.publish(vel_msg)
            return
        
        # 1 czesc funkcji - obracanie sie
        if abs(self.angular_vel(self.goal_pose,1)) >= EPSILON_ANG:
            vel_msg.linear.x = 0
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0

            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            vel = self.angular_vel(self.goal_pose,2)
            vel = max(-MAX_ANG_VEL, min(vel, MAX_ANG_VEL))  # bound velocity
            vel_msg.angular.z = vel

            self.velocity_publisher.publish(vel_msg)
        # 2 czesc funkcji - jechanie do przodu
        elif self.euclidean_distance(self.goal_pose) >= EPSILON_VEL:
            vel = self.linear_vel(self.goal_pose)
            vel = max(-MAX_LIN_VEL, min(vel, MAX_LIN_VEL))  # bound velocity 
            vel_msg.linear.x = vel
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0

            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            vel = self.angular_vel(self.goal_pose)
           
            vel = max(-MAX_ANG_VEL, min(vel, MAX_ANG_VEL))  # bound velocity
            vel_msg.angular.z = vel

            self.velocity_publisher.publish(vel_msg)
        # 3 czesc - jak osagnelismy cel, elo
        else:
            # Stopping our robot after the movement is over.
            vel_msg.linear.x = 0
            vel_msg.angular.z = 0
            self.velocity_publisher.publish(vel_msg)
            self.goal_reached = True


