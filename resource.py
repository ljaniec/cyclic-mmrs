#!/usr/bin/env python

# klasa modelujaca pojedynczy sektor, z lista zasobow zajmowanych w tym
# sektorze. Potrafi sprawdzac kolizje z innym, uzywane tylko w teselacji wiec
# malo istotne. 
class Resource:
    
    def __init__(self):
        self.tiles = []
        self.isPrivate = False

    def isColliding(self, res):
        for i in self.tiles:
            if i in res.tiles:
                return True
        return False

    def deleteRow(self, y_rising):
        if len(self.tiles) != 4:
            return
        if y_rising:
            y = min(self.tiles[0][1], self.tiles[1][1], self.tiles[2][1], self.tiles[3][1])
        else:
            y = max(self.tiles[0][1], self.tiles[1][1], self.tiles[2][1], self.tiles[3][1])
        for i in range(2):
            for r in self.tiles:
                if r[1] == y:
                    self.tiles.remove(r)

    def deleteColumn(self, x_rising):
        if len(self.tiles) != 4:
            return
        if x_rising:
            x = min(self.tiles[0][0], self.tiles[1][0], self.tiles[2][0], self.tiles[3][0])
        else:
            x = max(self.tiles[0][0], self.tiles[1][0], self.tiles[2][0], self.tiles[3][0])
        for i in range(2):
            for r in self.tiles:
                if r[0] == x:
                    self.tiles.remove(r)
                
            
