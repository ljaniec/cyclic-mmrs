#!/usr/bin/env python
import turtlebot as tb
import tesselation as tess
import resourcelist as rl
import world_generation as worldGen
from std_msgs.msg import UInt8
import rospy
import copy
import threading
import time
import matplotlib.pyplot as plt

### ALGORYTM 1 @@@@@@@@@@@@@@@@@@@@@@@@@@@

FLEET_SIZE = 6 # number of robots
R = 0.35 # m, radius of robot
D = 1.5   # m, size of square to tesselation

# global variables used by handle_request function
res = None
state = [0] * FLEET_SIZE
waiting = []
fleet = []
lock = threading.Lock()

def showPaths(paths):
    for path in paths:
        x = []
        y = []
        for point in path:
            x.append(point[0])
            y.append(point[1])
        plt.plot(x,y)
    plt.show()


# to jest ta logika najwyzsza, ktora na podstawie teselacji sprawdza czy robot
# moze przejechac do nastepnego sektora, nieistotne z punktu widzenia
# niskopoziomowego sterowania
def handle_request(id):
    lock.acquire()
    try:
        global state
        id = id.data
        new_state = copy.deepcopy(state)
        new_state[id] += 1
        print "robot ", id, " testuje stan ", new_state
        if new_state[id] == len(res.res[id]):
            new_state[id] -= 1
            fleet[id].goal_reached = True
            print "jest blad z przerostem state, proba odzyskania kontroli"
            return
        if res.isFeasible(new_state):  
            state = copy.deepcopy(new_state)
            fleet[id].allow()
            # za kazdym razem jak dajemy allow, znaczy, ze dla tego robota
            # zaczyna sie nowy etap ruchu. Zapisujemy sobie wtedy czas
            res.times[id].append(time.time())
            print "robot ", id, " uzyskal prawa przejazdu"
            # here we should check if some waiting vehicles aren't unlocked yet
            while True:
                flag = True
                for i in waiting:
                    new_state = copy.deepcopy(state)
                    new_state[i] += 1
                    if res.isFeasible(new_state):
                        flag = False
                        state = copy.deepcopy(new_state)
                        fleet[i].allow()
                        res.times[i].append(time.time())
                        waiting.remove(i)
                if flag:
                    break
        else:
            waiting.append(id)
            print "robot ", id, " nie uzyskal praw przejazdu"
    finally:
        lock.release()
            
# sprawdza czy przypadkiem wszystkie roboty juz nie skonczyly swoich zadan
def not_finished(fleet, path):
    for i in range(FLEET_SIZE):
        if (not fleet[i].stage_counter == len(path[i])) or (not fleet[i].goal_reached):
            return True
    return False

# ta funkcja opiera sie na turtlebot.move2goal, ktore dziala tak, ze jak zadasz
# jakis punkt, to turtlebot obroci sie w kierunku tego punktu, a potem
# podjedzie do niego po prostej. Cala funkcja iteruje sie po wszystkich
# turtlebotach. Jak ktorys osiagnal swoj punkt do ktorego mial jechac, to
# patrzy sie czy nie ma on jeszcze zaplanowanego odwiedzenia dalszych punktow,
# wszystkie punkty ma na tej liscie 'res' tworzonej w mainie, ale tez kazdy
# turtlebot osobno pamieta swoje wlasne, taka redundancja troche. Jak nie ma
# juz wiecej pkt to nic nie robi, a jak ma to zadaje mu kolejny punkt do
# ktorego ma jechac, i ustawia mu, ze teraz jest juz na kolejnym etapie ruchu.
# i tak az nie odwiedzi w koncu kazdy robot wszystkich swoich punktow
def actualize_goals(fleet, path):
    for i in range(FLEET_SIZE):
        if fleet[i].goal_reached:
            if fleet[i].stage_counter == len(path[i]):
                continue
            fleet[i].move2goal(path[i][fleet[i].stage_counter])
            fleet[i].stage_counter += 1

       
if __name__ == '__main__':
    try:
        name = 'tmp'
        workspaceX = 12 # m
        workspaceY = 12 # m
        robots = worldGen.worldGeneration(name, FLEET_SIZE, workspaceX, workspaceY, D)
        nPts = 6
        path = worldGen.pathsGeneration(robots, workspaceX, workspaceY, nPts, D)
        nRepeats = 0
        path = worldGen.addCycles(path, nRepeats)
        for it, p in enumerate(path):
            print 'Robot ' + str(it) + ':'
            print 'Start:', p[0]
            print 'Second:', p[1]
            print 'One before:', p[-2]
            print 'End:', p[-1]
            print p
            print '***************'
        # mierzenie czasu zeby bylo w wynikach
        start = time.time()

        # Creates a node with name 'turtlebot_controller' and make sure it is a
        # unique node (using anonymous=True).
        rospy.init_node('turtlebot_controller', anonymous=True)
        request_subscriber = rospy.Subscriber('/crossing_requests', UInt8, handle_request) 
        # tutaj zadaje sciezki dla wszystkich robotow, tak jak opisalem w pracy
        # path = []
        # first element of path should be the vehicle starting position
        # path.append([(2.72, 0.23),  (2.66, 2.67), (0.29, 2.73)])
        # path.append([(4.33, 0.3),  (4.28, 1.64), (0.42, 1.74)])
        # path.append([(1.88, 4.32), (1.66, 0.39), (0.32, 0.59)])
        # path.append([(4.16, 4.3),  (4.3, 3.34), (5.63, 3.33)])
        # path.append([(5.43, 1.79), (2.9, 1.79), (1.62, 4.31)])
        # path.append([(3.39, 4.29),  (3.39, 2.68), (5.61, 0.23)])
        showPaths(path)
        # tutaj te sciezki sa mielone przez funkcje, i wypluwa mi ona jakie
        # sektory bedzie kolejno zajmowal kazdy robot, i jakie zasoby musi
        # sobie zaalokowac w tych sektorach. Takie tabelki jak w tej pracy z
        # 2011
        res = rl.ResourceList( tess.path2resourceList(path, R, D) )
        res.display()
        

        # tworzenie obiektow klasy turtlebot, i zadawanie im od razu ich list
        # sektorow, ktore stworzylem przed chwila
        for i in range(FLEET_SIZE):
            fleet.append(tb.TurtleBot(i,R,D,res.res[i]))

        # zaczynami liczyc czasy
        res.startCountingTime()

        # aktualizowanie celow - opis tej funkcji wyzej
        actualize_goals(fleet, path)
        print "zadalem cele"
        
        # dopoki kazdy nie skonczy swojego zadania, aktualizuj im cele co pol
        # sekundy
        while not_finished(fleet, path):
            actualize_goals(fleet, path)
            rospy.sleep(0.5)
        # mierzenie czasu zeby bylo w wynikach
        end = time.time()
        print "Time elapsed:", end - start, "s"
        res.displayTimes()
        
    except rospy.ROSInterruptException:
        pass
